using System;
using System.Collections.Concurrent;
using System.Threading;

namespace hw_7
{
    public class ToParallel : Sum
    {
        public override void GetSum(int num)
        {
            ConcurrentQueue<int> mass = new();
            Parallel.For(0, num, x => mass.Append(x));
        }
    }
}
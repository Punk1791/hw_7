﻿using System;

namespace hw_7
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var norm = new Normal();
            var thr = new ToThread();
            var par = new ToParallel();

            norm.GetTimer("Обычный", 100000);
            norm.GetTimer("Обычный", 1000000);
            norm.GetTimer("Обычный", 10000000);

            thr.GetTimer("Thread", 100000);
            thr.GetTimer("Thread", 1000000);
            thr.GetTimer("Thread", 10000000);

            par.GetTimer("Parallel", 100000);
            par.GetTimer("Parallel", 1000000);
            par.GetTimer("Parallel", 10000000);
        }
    }
}
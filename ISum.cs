using System;

namespace hw_7
{
    public abstract class Sum
    {
        public abstract void GetSum(int num);

        public void GetTimer(string method, int num)
        {
            var _to = DateTime.Now;
            GetSum(num);
            Console.WriteLine($"Время метода {method}, количество {num}: {DateTime.Now - _to}");
        }
    }
}
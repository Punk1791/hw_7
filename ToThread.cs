using System;
using System.Collections.Concurrent;
using System.Threading;

namespace hw_7
{
    public class ToThread : Sum
    {
        public override void GetSum(int num)
        {
            ConcurrentQueue<int> mass = new();
            var threads = new List<Thread>();
            int paginate = 10;

            for(int i = 1; i < paginate; i++)
            {
                Thread thr = new(() =>
                    {
                        for(int j = (num/10)*(i-1); j < (num/10)*i; j++)
                        {
                            mass.Append(j);
                        }
                    }
                );
                threads.Add(thr);
            }

            var check = false;
            var forcheck = true;
            while (forcheck)
            {
                foreach(var i in threads)
                {
                    check = forcheck || i.IsAlive;
                    forcheck = i.IsAlive;
                }
            }
        }
    }
}